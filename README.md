### How to use

1. Show behaviour without composite primary key:

   ```shell
   docker compose up
   ```

1. Show behaviour with composite primary key:

   ```shell
   COMPOSITE_PRIMARY_KEY=true docker compose up
   ```
