#!/usr/bin/env ruby

require "bundler/inline"

gemfile(true) do
  source "https://rubygems.org"
  git_source(:github) { |repo| "https://github.com/#{repo}.git" }
  gem "factory_bot", "~> 6.0"
  gem "activerecord"
  gem "pg"
end

require "active_record"
require "factory_bot"
require "minitest/autorun"
require "logger"

POSTGRESQL_CONNECTION_STRING="postgresql://postgres:mysecretpassword@database:5432/factorybot_package_versions_test"
ActiveRecord::Base.establish_connection(POSTGRESQL_CONNECTION_STRING)

ActiveRecord::Base.logger = Logger.new(STDOUT)

ActiveRecord::Schema.define do
  create_table :packages do |t|
    t.integer :purl_type, null: false, limit: 2
    t.text :name, null: false, limit: 255
    t.index [:purl_type, :name], unique: true, name: 'i_packages_purl_type_and_name'
  end
  create_table :package_versions do |t|
    t.references :package, foreign_key: { to_table: :packages, on_delete: :cascade }
    t.text :version, null: false, limit: 255
    t.index [:package_id, :version], unique: true, name: 'i_package_versions_on_package_id_and_version'
  end
  create_table :licenses do |t|
    t.text :spdx_identifier, null: false, limit: 50
    t.index [:spdx_identifier], unique: true, name: 'i_licenses_on_spdx_identifier'
  end

  if ENV['COMPOSITE_PRIMARY_KEY'] != ""
    create_table :package_version_licenses, primary_key: [:package_version_id, :license_id] do |t|
      t.references :package_version, foreign_key: { on_delete: :cascade }, null: false
      t.references :license, foreign_key: { on_delete: :cascade }, null: false
    end
  else
    create_table :package_version_licenses do |t|
      t.references :package_version, foreign_key: { on_delete: :cascade }, null: false
      t.references :license, foreign_key: { on_delete: :cascade }, null: false
    end
  end
end

class Sbom
  PURL_TYPES = { composer: 1, conan: 2, gem: 3, golang: 4, maven: 5,
                 npm: 6, nuget: 7, pypi: 8 }.with_indifferent_access.freeze

  def self.purl_types
    PURL_TYPES
  end
end

class PackageVersion < ActiveRecord::Base
  belongs_to :package, foreign_key: :package_id, inverse_of: :package_versions, optional: false
  has_many :package_version_licenses, inverse_of: :package_version, foreign_key: :package_version_id
  has_many :licenses, through: :package_version_licenses

  validates :version, presence: true, length: { maximum: 255 }
end

class License < ActiveRecord::Base
  validates :spdx_identifier, presence: true, length: { maximum: 50 }

  has_many :package_version_licenses, inverse_of: :license, foreign_key: :license_id
end

class Package < ActiveRecord::Base
  enum purl_type: Sbom.purl_types

  validates :purl_type, presence: true
  validates :name, presence: true, length: { maximum: 255 }

  has_many :package_versions, inverse_of: :package, foreign_key: :package_id
end

class PackageVersionLicense < ActiveRecord::Base
  belongs_to :package_version,
      foreign_key: :package_version_id,
      inverse_of: :package_version_licenses,
      optional: false

  belongs_to :license,
      foreign_key: :license_id,
      inverse_of: :package_version_licenses,
      optional: false
end

FactoryBot.define do
  factory :package, class: 'Package' do
    purl_type { :npm }
    sequence(:name) { |n| "package-#{n}" }

    transient do
      version { '1.0.0' }
      spdx_identifiers { [] }
    end

    initialize_with do
      Package.where(name: name, purl_type: purl_type).first_or_initialize
    end

    package_versions do
      [association(:package_version, version: version,
                   spdx_identifiers: spdx_identifiers, package: instance)]
    end
  end

  factory :package_version, class: 'PackageVersion' do
    sequence(:version) { |n| "v0.0.#{n}" }

    transient do
      spdx_identifiers { [] }
    end

    package { association :package }

    package_version_licenses do
      spdx_identifiers.map do |spdx_identifier|
        association(:package_version_license, package_version: instance,
                    license: association(:license, spdx_identifier: spdx_identifier))
      end
    end
  end

  factory :license, class: 'License' do
    sequence(:spdx_identifier) { |n| "OLDAP-2.#{n}" }

    initialize_with { License.where(spdx_identifier: spdx_identifier).first_or_create! }
  end

  factory :package_version_license, class: 'PackageVersionLicense' do
    package_version { association :package_version }
    license { association :license }
  end
end

# Override strategy method so we can see what happens under the hood.
module FactoryBot
  def self.create(*args, **kwargs)
    puts "calling create with args: #{args} and kwargs: #{kwargs}"
    super
  end
end

class FactoryBotTest < Minitest::Test
  def test_factory_bot_lint
    FactoryBot.factories.each do |factory|
      puts
      puts "Lint '#{factory.name}'"
      puts
      ActiveRecord::Base.transaction do
        FactoryBot.lint([factory]) # passes
        raise ActiveRecord::Rollback
      end
    end
  end

  def test_factory_bot_lint_with_traits
    FactoryBot.factories.each do |factory|
      puts
      puts "Lint '#{factory.name}' with traits"
      puts
      ActiveRecord::Base.transaction do
        FactoryBot.lint([factory], traits: true) # fails for approved_post
        raise ActiveRecord::Rollback
      end
    end
  end

  def test_create_packages
    FactoryBot.create(:package, name: "beego", purl_type: "golang", version: "v1.10.0",
           spdx_identifiers: ["OLDAP-2.1", "OLDAP-2.2"])
    FactoryBot.create(:package, name: "camelcase", purl_type: "npm", version: "1.2.1", spdx_identifiers: ["OLDAP-2.1"])
    FactoryBot.create(:package, name: "camelcase", purl_type: "npm", version: "4.1.0", spdx_identifiers: ["OLDAP-2.2"])
    FactoryBot.create(:package, name: "cliui", purl_type: "npm", version: "2.1.0", spdx_identifiers: ["OLDAP-2.3"])
    FactoryBot.create(:package, name: "cliui", purl_type: "golang", version: "2.1.0", spdx_identifiers: ["OLDAP-2.6"])
    FactoryBot.create(:package, name: "jst", purl_type: "npm", version: "3.0.2", spdx_identifiers: ["OLDAP-2.4", "OLDAP-2.5"])
    FactoryBot.create(:package, name: "jsbn", purl_type: "npm", version: "0.1.1", spdx_identifiers: ["OLDAP-2.4"])
    FactoryBot.create(:package, name: "jsdom", purl_type: "npm", version: "11.12.0", spdx_identifiers: ["OLDAP-2.5"])
  end
end
